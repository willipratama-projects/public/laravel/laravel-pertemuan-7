<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index()
    {
        // dikelola oleh LandingController
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Portfolio $portfolio)
    {
        return view('contents.portfolio.detail', compact('portfolio'));
    }

    public function edit(Portfolio $portfolio)
    {
        //
    }

    public function update(Request $request, Portfolio $portfolio)
    {
        //
    }

    public function destroy(Portfolio $portfolio)
    {
        //
    }
}
