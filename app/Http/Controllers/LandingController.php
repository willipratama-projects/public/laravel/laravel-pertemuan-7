<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portfolio;

class LandingController extends Controller
{
    public function index()
    {
        return view('contents.index');
    }

    public function portfolios()
    {
        $portfolios = Portfolio::all();
        $pageName = 'Portfolios';
        return view(
            'contents.portfolio.index',
            compact(
                [
                    'portfolios',
                    'pageName',
                ]
            )
        );
    }

    public function about()
    {
        $pageName = 'About';
        return view('contents.about', compact('pageName'));
    }
}
