<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DefaultPortfolioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('portfolios')->insert([
            'name' => 'Log Cabin',
            'image' => 'cabin.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.',
            'created_at' => Carbon::now(),
        ]);

        DB::table('portfolios')->insert([
            'name' => 'Tasty Cake',
            'image' => 'cake.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.',
            'created_at' => Carbon::now(),
        ]);

        DB::table('portfolios')->insert([
            'name' => 'Circus Tent',
            'image' => 'circus.png',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia neque assumenda ipsam nihil, molestias magnam, recusandae quos quis inventore quisquam velit asperiores, vitae? Reprehenderit soluta, eos quod consequuntur itaque. Nam.',
            'created_at' => Carbon::now(),
        ]);
    }
}
