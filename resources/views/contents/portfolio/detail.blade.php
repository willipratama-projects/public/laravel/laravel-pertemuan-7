@extends('layouts.app')

@section('content')
<section class="page-section text-center mb-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <!-- Portfolio Modal - Title-->
                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0">{{ $portfolio->name }}</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Portfolio Modal - Image-->
                <img class="img-fluid rounded mb-5" src="{{ asset('assets/img/portfolio/' . $portfolio->image) }}" alt="{{ $portfolio->name }}" />
                <!-- Portfolio Modal - Text-->
                <p class="mb-4">{{ $portfolio->description }}</p>
            </div>
        </div>
    </div>
</section>
@endsection
