<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingController@index')->name('index');
Route::get('/portfolios', 'LandingController@portfolios')->name('portfolios');
Route::get('/portfolios/{portfolio}', 'PortfolioController@show')->name('portfolios.show');
Route::get('/about', 'LandingController@about')->name('about');
